import RestAdapter from '@ember-data/adapter/rest';

export default class ProjectsAdapter extends RestAdapter {
  //host = 'https://api.october.eu';
  namespace = 'api';
  buildURL(...args) {
    return `${super.buildURL(...args)}.json`;
  }
}
