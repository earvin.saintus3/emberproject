import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class ProjectsRoute extends Route {
  @service store;

  async model() {
    const response = await fetch('/api/projects.json');
    const { projects } = await response.json();
    console.log(projects);
    return projects;
  }

  //gestion du text en fr
  // setupController(controller, model) {
  //   super.setupController(controller, model);
  //   controller.value = model.summary.fr[0].value;
  // }
}
