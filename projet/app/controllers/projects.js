import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class ProjectsController extends Controller {
  @tracked state = this.model.status;

  get projectStatus() {
    return this.model.status.find(({ state }) => state === this.status);
  }
}
