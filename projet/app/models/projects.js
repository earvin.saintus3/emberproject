import Model, { attr } from '@ember-data/model';

export default class ProjectsModel extends Model {
  @attr status;
  @attr name;
  @attr summary;
  @attr loanDuration;
  @attr amount;
  @attr totalInvested;
  @attr rate;
  @attr grade;
  @attr rate;
  @attr illustration;
  @attr business;
  @attr signatory;
}
